# SMAI

This repository contains files for Statistical Methods in AI course taken in Monsoon 2020 semester.

## Contents

1. Homeworks
2. Quizzes
3. Review Questions
4. In class Problem Solving Questions
5. Additional Material (notes, etc.)

For Homeworks, submitted files are uploaded. The published key is also uploaded.

For Review questions, the user-made questions (not made by me) with answers (as given by the key) document, along with the explanation docs (might not have the correct answer) are posted.

For Quiz questions, the key is uploaded.

For the problem solving questions, the question+answer doc is uploaded.

Additional material will be made later.
